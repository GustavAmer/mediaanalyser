package demo;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement
public class Item {
	
	String title;
	String link;
	String pubDate;
	String description;
	String sentiment;
	ArrayList<String> keywords = new ArrayList<String>();
}
