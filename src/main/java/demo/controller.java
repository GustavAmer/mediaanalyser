package demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class controller {
	
	List<Item> items;
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/analyze/{keys}", method=RequestMethod.GET)
	public List<SentimentCount> analyze(@PathVariable("keys") String keys){
		String[] s = keys.split(" ");
		ArrayList<String> mKeys = new ArrayList<String>();
		for(int i = 0; i < s.length; i++){
			String[] t = s[i].split(",");
			for(int j = 0; j < t.length; j++){
				mKeys.add(t[j].toLowerCase());
			}
		}
		if(items == null)
			items = getFeeds(); 
		ResponseParser r = new ResponseParser();
		return r.sort(items, mKeys);
	}
	
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value="/feeds", method=RequestMethod.GET)
	public void feeds(){
		new Thread(){
			@Override
			public void run(){
				items = getFeeds(); 
			}
		}.start();
		return;
	}
	
	public List<Item> getFeeds(){
		List<Item> items = XMLParser.readXML(getRequestResult("http://www.postimees.ee/rss"));
		items.addAll(XMLParser.readXML(getRequestResult("http://www.pealinn.ee/rss.php?type=news")));
		items.addAll(XMLParser.readXML(getRequestResult("http://feeds.feedburner.com/delfiuudised?format=xml")));
		items.addAll(XMLParser.readXML(getRequestResult("http://www.aripaev.ee/mod/rss.xml")));
		items.addAll(XMLParser.readXML(getRequestResult("http://feeds.feedburner.com/eestipaevaleht?format=xml")));
		items.addAll(XMLParser.readXML(getRequestResult("http://www.ohtuleht.ee/rss")));
		items.addAll(XMLParser.readXML(getRequestResult("http://uudised.err.ee/rss")));
		items.addAll(XMLParser.readXML(getRequestResult("http://feeds.feedburner.com/EestiEkspressFeed?format=xml")));
		
		for (int i = 0; i < items.size();i ++){
			items.get(i).setSentiment(getSentiment(items.get(i).getDescription()));
		}
		System.out.println("got feeds");
		return items;
	}
	
	public String getRequestResult(String url){
        String result;
		URL https_url;
		HttpURLConnection httpCon;
		try{
			https_url = new URL(url);
			URLConnection connection = https_url.openConnection();			
			httpCon = (HttpURLConnection) connection;
			httpCon.setRequestMethod("GET");
			httpCon.connect();
			result = convertStreamToString(httpCon.getInputStream());
			httpCon.disconnect();
		}catch (Exception e) {
			System.out.print("error:" + e);
			result = "";
		}
		return result;
	}
	
	public final String getSentiment(String data) {
	
		data = "dataonly=2&text="+data;		
		URL https_url = null;
		HttpURLConnection httpCon = null;
		String result = "";
		try{
			https_url = new URL("http://peeter.eki.ee:5000/valence/color");
			URLConnection connection = https_url.openConnection();
			httpCon = (HttpURLConnection) connection;
			httpCon.setDoInput (true);
			httpCon.setDoOutput (true);
			httpCon.setUseCaches (false);
			httpCon.setInstanceFollowRedirects(false); 
			httpCon.setRequestMethod("POST");
			httpCon.connect();
			OutputStreamWriter out = new   OutputStreamWriter(httpCon.getOutputStream());
		    out.write(data);
		    out.close();
            result = convertStreamToString(httpCon.getInputStream());
		    httpCon.disconnect();		
		}catch (Exception e) {
			result = "";
		}
		return result;
	}
	
	  @Scheduled(fixedRate = 8 * 60 * 60 * 60 * 1000)
	  public void reportCurrentTime() {
		  System.out.println("scheduled task");
		  items = getFeeds();
	    }
	  
	public static String convertStreamToString(InputStream is) {
		if(is == null)
			return "";

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
		    while ((line = reader.readLine()) != null) {
		        sb.append(line + "\n");
		    }
		} catch (IOException e) {
		} finally {
		    try {
		        is.close();
		    } catch (IOException e) {
		    }
		}
		return sb.toString();
	}	
}