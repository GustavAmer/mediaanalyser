package demo;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XMLParser {
  static final String TITLE 		= "title";
  static final String ITEM 			= "item";
  static final String LINK		 	= "link";
  static final String SOURCE_URL	= "source url";
  static final String PUB_DATE		= "pubDate";
  static final String DESCRIPTION	= "description";


  public static List<Item> readXML(String source) {
    List<Item> items = new ArrayList<Item>();
    Pattern p = Pattern.compile("[^\\u0009\\u000A\\u000D\u0020-\\uD7FF\\uE000-\\uFFFD\\u10000-\\u10FFF]+");
    source = p.matcher(source).replaceAll("");
    try {
      XMLInputFactory inputFactory = XMLInputFactory.newInstance();
      InputStream in= new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8));
      XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
      Item item = null;
      while (eventReader.hasNext()) {
    	  XMLEvent event = eventReader.nextEvent();
	      try{
	    	  if (event.isStartElement()) {
	    		  StartElement startElement = event.asStartElement();
	    		  if (startElement.getName().getLocalPart() == (ITEM)) {
	    			  item = new Item();
	    		  }
		          if (event.asStartElement().getName().getLocalPart()
		              .equals(TITLE)) {
		            event = eventReader.nextEvent();
		            if(item != null)
		            	item.setTitle(event.asCharacters().getData());
		            continue;
		          }
		          if (event.asStartElement().getName().getLocalPart().equals(LINK)) {
		        	  event = eventReader.nextEvent();
		              if(item != null)
		            	  item.setLink(event.asCharacters().getData());
		        	  continue;
		          }
		          if (event.asStartElement().getName().getLocalPart().equals(DESCRIPTION)) {
		        	  event = eventReader.nextEvent();
		              if(item != null)
		            	  item.setDescription(event.asCharacters().getData());
		        	  continue;
		          }
		          if (event.asStartElement().getName().getLocalPart().equals(PUB_DATE)) {
		        	  event = eventReader.nextEvent();
		        	  item.setPubDate(event.asCharacters().getData());
		        	  continue;
		          }
		        }
		        if (event.isEndElement()) {
		          EndElement endElement = event.asEndElement();
		          if (endElement.getName().getLocalPart() == (ITEM)) {
		            items.add(item);
		          }
		        }
        }catch(Exception e){
        	
        }
      }
    } catch (XMLStreamException e) {
      e.printStackTrace();
    }
    return items;
  }

  public static String getArticlePM(String source) {
	  String[] s = source.split("itemprop=\"articleBody\">");
	  return s[1].split("<div")[0];
  }
  
} 