package demo;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@XmlRootElement
@Data
public class SentimentCount {
	
	
	String keywords;
	int posCount = 0;
	int negCount = 0;
	int neuCount = 0;
	
	public SentimentCount(String keys, String sentiment){
		keywords = keys;
		if(sentiment.contains("neg"))
			negCount = 1;
		else if(sentiment.contains("pos"))
			posCount = 1;
		else if(sentiment.contains("neu"))
			neuCount = 1;
	}
	
	public void update(String sentiment){
		if(sentiment.contains("neg"))
			negCount++;
		else if(sentiment.contains("pos"))
			posCount++;
		else if(sentiment.contains("neu"))
			neuCount++;
	}

}
