package demo;

import java.util.ArrayList;
import java.util.List;


public class ResponseParser {
	
	ArrayList<SentimentCount> response = new ArrayList<SentimentCount>();
	
	public final static String neg = "negatiivne";
	public final static String neu = "neutraalne";
	public final static String pos = "positiivne";
	
	public ArrayList<SentimentCount> sort(List<Item> items, ArrayList<String> keywords){
		ArrayList<Item> mItems = new ArrayList<Item>();
		
		for(Item item: items){
			item.keywords.clear();
			try{
				item.description = item.description.toLowerCase();
				for(String key: keywords){
					if(item.description.contains(key)){
						item.keywords.add(key);
					}
				}
			}catch(NullPointerException e){}
			if(item.keywords.size() > 0){
				mItems.add(item);
				if(response.size() == 0)
					response.add(new SentimentCount(item.keywords.toString().replaceAll("\\[", "").replaceAll("\\]",""), item.sentiment));
				else{
					boolean exists = false;
					for(SentimentCount sc: response){
//						System.out.println(item.keywords.toString() + "  " + sc.keywords + " " + item.keywords.toString().equals(sc.keywords));
						if(item.keywords.toString().replaceAll("\\[", "").replaceAll("\\]","").equals(sc.keywords)){
							sc.update(item.sentiment);
							exists = true;
							break;
						}
					}if(!exists)
						response.add(new SentimentCount(item.keywords.toString().replaceAll("\\[", "").replaceAll("\\]",""), item.sentiment));
				}			
			}
		}
		return response;
	}
}

