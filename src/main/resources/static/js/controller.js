var app = angular.module('project');

app.controller('Controller', function($scope, $http, $location) {

	$scope.articles = [];
    $scope.analyze = function () {
    	var keys = document.getElementById("keyWords").value;
	    console.log(keys);
        $http.get('/analyze/' + keys).success(function(data, status, headers, config) {
	        console.log(data);
	        $scope.articles = data;
	    });
    };
    $scope.feeds = function () {
        $http.get('/feeds')
    };
});