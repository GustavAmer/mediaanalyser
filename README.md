# Media Analyzer #

This repository contains source code of a media analyzer. 
This analyzer was created as coursework for University of Tartu course Seminar on Enterprise Software.

### Overview ###

Application is written in Java with [Spring Tool Suite](https://spring.io/tools).
It is a web application, which gathers   from multiple Estonian news sites and calculates their sentiment using [EKI emotion detector](http://peeter.eki.ee:5000/valence). 
Front-end enables retrieving information about articles containing desired key words.

### How do I get set up? ###
Checkout the source from the repository and import it into [Spring Tool Suite](https://spring.io/tools).
Then run it from the development environment. 

###Application functionality###
Application retrieves RSS feeds from Estonian news sites. Retrieval process is scheduled to be triggered every 8 hours. It is also triggered, if a request is received and no new articles are saved. 
After retrieving the feeds, the articles' sentiment is set according to the response from *EKI Emotsioonidetektor*.


When a request is received, all the saved articles are searched for any of the keywords. Eventually all found key word combinations together with counts of articles, where they were found, sorted by sentiment, are returned.

### Who do I talk to? ###

* Repo owner: [Gustav Amer](gustavamer@gmail.com)