var app = angular.module("project", ["ngRoute"]);

app.config(function($routeProvider) {
	$routeProvider
	.when('', {
	    controller: 'Controller',
	    templateUrl: 'views/analyze.html'
	}).otherwise({
		controller: 'Controller',
	    templateUrl: 'views/analyze.html'			 
	})
})
